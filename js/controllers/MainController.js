app.controller('MainController', ['$scope', function($scope) {
  $scope.values = [];
  $scope.date = [];
  $scope.insert = function() {
    var val = $scope.value;
    var date = new Date();
    var month, days, hours, minutes, seconds;

    ((date.getMonth() + 1) < 10) ? month = ('0' + (date.getMonth() + 1)) : month = (date.getMonth() + 1);
    (date.getDate() < 10) ? days = ('0' + date.getDate()) : days = (date.getDate());
    (date.getHours() < 10) ? hours = ('0' + date.getHours()) : hours = (date.getHours());
    (date.getMinutes() < 10) ? minutes = ('0' + date.getMinutes()) : minutes = (date.getMinutes());
    (date.getSeconds() < 10) ? seconds = ('0' + date.getSeconds()) : seconds = (date.getSeconds());

    var now = date.getFullYear() + '.' + month + '.' + days + ' ' + hours + ':' + minutes + ':' + seconds;
    //using {{date | date}} won't provide me expected result, so I have to do it like this /\

    if(isNaN(val) || val === '') {
      alert('Wrong value!');
    } else {
      var randomFrom0to100 = Math.floor((Math.random() * 100) + 1);
      var finalValue = (val * 4.2 * (1+(randomFrom0to100 - 50)/1000)).toFixed(2);

      $scope.values.push(finalValue);
      $scope.date.push(now);
    }
  };
}]);
